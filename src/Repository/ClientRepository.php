<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findClientOnMainServer($user) : array
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.email = :email')
                ->andWhere('c.id_passport = :id_passport')
                ->setParameter('email', $user['email'])
                ->setParameter('id_passport', $user['idPassport'])
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function checkUserForLogin($user): array
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.email = :email')
                ->andWhere('c.password = :password')
                ->setParameter('email', $user['email'])
                ->setParameter('password', $user['password'])
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}

<?php

namespace App\Controller;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="ping")
     */
    public function pingAction()
    {
        return new JsonResponse(["Pong"]);
    }
    /**
     * @Route("/verification-client", name="verification")
     */
    public function verificationClintAction(Request $request, ClientRepository $clientRepository)
    {
        $user = $request->query->get('userData');
        $resultSql = $clientRepository->findClientOnMainServer($user);



        if (!empty($resultSql)) {
//            return new JsonResponse(['This is Client exist in our server']);
            return new JsonResponse($resultSql);
        }else{
            $this->registrationClientAction($user);
        }

        return new JsonResponse($resultSql);
    }

    public function registrationClientAction($clientData){
        $client = new Client();
        $client
            ->setEmail($clientData['email'])
            ->setPassword($clientData['password'])
            ->setIdPassport($clientData['idPassport']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();
    }

    /**
     * @Route("/try-login")
     * @param Request $request
     * @param ClientRepository $clientRepository
     */
    public function checkUserForLogin(Request $request, ClientRepository $clientRepository)
    {
        $user = $request->query->get('userData');
        $resultSql = $clientRepository->checkUserForLogin($user);

        if (!empty($resultSql)) {
            return new JsonResponse($resultSql);
        }

        return new JsonResponse($resultSql);
    }
}
